<?php

class GaitameOnline {
	private string $rawjson;
	private array $data;
	private bool $success;

	public const CURRENCY_USD_JPY = 'usdjpy';
	public const CURRENCY_EUR_JPY = 'eurjpy';

	private const API_URL = 'https://www.gaitameonline.com/rateaj/getrate';

	private const VALUE_TYPE_BID = 'bid';		// 売値
	private const VALUE_TYPE_ASK = 'ask';		// 買値
	private const VALUE_TYPE_OPEN = 'open';		// 始値
	private const VALUE_TYPE_HIGH = 'high';		// 高値
	private const VALUE_TYPE_LOW = 'low';		// 安値

	function __construct ()
	{
		$this->rawjson = '{}';
		$this->success = false;

		$ci = curl_init(self::API_URL);
		curl_setopt($ci, CURLOPT_RETURNTRANSFER, true);
		if ( ($result = curl_exec($ci)) !== FALSE ) {
			$this->rawjson = $result;
			$this->success = true;
			$this->data = json_decode($this->rawjson, true);
		}
		curl_close($ci);
	}

	public function get_bid ( $currency = self::CURRENCY_USD_JPY ): ?string
	{
		return $this->get_value(self::VALUE_TYPE_BID, $currency);
	}

	public function get_ask ( $currency = self::CURRENCY_USD_JPY ): ?string
	{
		return $this->get_value(self::VALUE_TYPE_ASK, $currency);
	}

	private function get_value ( $type, $currency ): ?string
	{
		if ( !$this->success ) {
			return null;
		}

		$value = null;
		foreach ( $this->data['quotes'] as $quote ) {
			if ( mb_strpos(strtolower($quote['currencyPairCode']), $currency) === false ) {
				continue;
			}
			$value = $quote[$type];
		}

		return $value;
	}
}
