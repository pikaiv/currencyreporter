<?php
spl_autoload_register(function ($class_name) {
	include 'src' . $class_name . '.php';
});

$go = new GaitameOnline();

$usdvalues = array(floatval($go->get_bid(GaitameOnline::CURRENCY_USD_JPY)), floatval($go->get_ask(GaitameOnline::CURRENCY_USD_JPY)));
$val['usdjpy'] = round(array_sum($usdvalues) / count($usdvalues), 2);

$eurvalues = array(floatval($go->get_bid(GaitameOnline::CURRENCY_EUR_JPY)), floatval($go->get_ask(GaitameOnline::CURRENCY_EUR_JPY)));
$val['eurjpy'] = round(array_sum($eurvalues) / count($eurvalues), 2);

$toot = 'USDJPY: '. $val['usdjpy'] . PHP_EOL;
$toot .= 'EURJPY: '. $val['eurjpy'];

// とぅーとする
require_once("MastodonClient/MastodonClient.php");
$mc = new MastodonClient();
$mc->init();
$mc->post_statuses(MastodonClient::VISIBILITY_UNLISTED, $toot);

